<?php get_header();
//Traigo los datos de usuario loggeado
$cu= wp_get_current_user();

//Extraigo el id del usuario
$id = $cu->id;
//
//Llamo a la funcion que trae datos de la tabla _wpusermeta la columna municipio y le paso el id del usuario 
$municipio = get_the_author_meta('municipio', $user = $id );
$id_area = get_the_author_meta('area', $user = $id );
//echo $municipio;

global $wpdb;
$resultados= $wpdb->get_results( "SELECT *FROM periodo where id_area = $id_area" );

?>

<div class="container"> 
	<div id="grid">
        <form name="formFac" id="formFac" action="" method="POST">
        <div class="toolbar"> 
             <input type="button" id="btnAgregarItem" name="btnAgregarItem"  class="btn btn-success pull-right" value="Agregar periodo" onclick="openWindow()" />       
        </div>
        </form>
    </div>
</div>

<div id="windowFormItem" style="display:none">
        <form id="formCatalogo" action="" method="">
            <div class="container sin-padding">
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="form-group no-margen">
                        <label class="control-label" for="pwd">Periodo:</label>
                         <input type="text" id="nombre_periodo" name="nombre_periodo" class="form-control" placeholder="Nombre periodo" required onKeyUp="this.value=this.value.toUpperCase();"/>
                    </div> 
                    <div class="form-group no-margen">
                        <label class="control-label" for="pwd">Rango inicial: </label> <!--Nombre -->
                        <input type="hidden" id="id_periodo" name="id_periodo"/> 
                        <input type="text" id="rango_inicial" name="rango_inicial" class="form-control" placeholder="Rango inicial" required onKeyUp="this.value=this.value.toUpperCase();"/>
                    </div>
                    <div class="form-group no-margen">
                        <label class="control-label" for="pwd">Rango final:</label>
                         <input type="text" id="rango_final" name="rango_final" class="form-control" placeholder="Rango final" required onKeyUp="this.value=this.value.toUpperCase();"/>
                    </div> 
                    
                </div>
                   
            </div>
        </form>
        <div class="control-group">
            <div class="controls" style="text-align:center">
                <input id="btnCancelar" type="button" value="Cancelar" class="btn btn-default" onclick="closeWindow()" />
                <input id="btnGuardar" type="button" value="Guardar cambios" class="btn btn-primary" onclick="guardaPeriodo()" />
            </div>
        </div>
</div>


<script type="text/javascript">  

var objItem;

function openWindow() {
    limpiarInputs();
    //validator.hideMessages();
    objItem = new Object();
    objItem.campos = new Array();
    //objItem.activo = 1;
    $("#id_periodo").val("0");

    var popup = $("#windowFormItem").data("kendoWindow");
    //var ancho = (anchoVentana * 0.9);
    popup.setOptions({
        title: 'Editar periodo',
        width: 600
    });
    $("#windowFormItem").data("kendoWindow").center().open();
}

function closeWindow() {
   /* if(objItem !== undefined && objItem.tieneCambios !== undefined && objItem.tieneCambios == true){
        if(confirm("El inventario ha sido modificado. ¿Desea guardar los cambios?")){
    $("#windowFormItem").data("kendoWindow").close();
            objItem.tieneCambios = false;
            envioForm();
        }else{
            limpiarInputs();
            $("#windowFormItem").data("kendoWindow").close(); 
    }
    }else{
        limpiarInputs();*/
        $("#windowFormItem").data("kendoWindow").close();  
    //}
   
}

function limpiarInputs(){
    document.getElementById("formCatalogo").reset();
} 

function guardaPeriodo() {
    //$('.k-loading-mask').removeClass("display-nones");
    if (objItem.id_periodo === undefined)
        objItem.id_periodo = 0;
    if (validator.validate()) {
        //if( $('#formCatalogo').bootstrapValidator()){
        objItem.tieneCambios = false;
        
        objItem.nombre_periodo = $("#nombre_periodo").val().toUpperCase();
        objItem.rango_final = $("#rango_final").val().toUpperCase();
        objItem.rango_inicial = $("#rango_inicial").val().toUpperCase();
        
         
        $.ajax({
            type: "POST",
            url: urlInventario,
            dataType: "json",
            data: {inventario: JSON.stringify(objItem)},
            success: function(response){
                //var respuesta = $.parseJSON(response);
                 if(response.success == 1){
                    //$('.k-loading-mask').addClass("display-nones");
                    alert(response.msg);
                    closeWindow();
                    //$("#tablaItems").data("kendoGrid").refresh();
                   // $(".k-pager-refresh").trigger('click');
                    //pintaGrid();
                   // $("#tablaItems").data("kendoGrid").dataSource.sync();
                    //$("#tablaItems").data("kendoGrid").refresh();
                            
                 }else{
                    // $('.k-loading-mask').addClass("display-nones");
                    alert(response.msg);
                    pintaGrid();
                }
            }
        });
        
    }
}
	 
$(document).ready(function () {  

    $("#windowFormItem").kendoWindow({
                width: '0px',
                height: 'auto',
                resizable: false,
                modal: true,
                visible: false,
                animation: false,
                actions: {}
            });
             

	$("#grid").kendoGrid({
        dataSource: {
            sync: function(){
                this.read();
            },
            transport: {
                read: {
                    url: "http://localhost/wordpress/api-municipios/?acc=data",
                    type: "GET",
                    dataType: "json" 
                }
            },
            pageSize: 10,
            schema: {
                type: "json",
                data: "data",
                model: {
                    id: "id_periodo",
                    fields: {
                        nombre: {type: "number"}
                    }
                },
                total: function(e) {
                   // handle event
                   var it_works = 0;
                   $.ajax({
                      type: 'GET',
                      url: 'http://localhost/wordpress/api-municipios/?acc=total',
                      async: false, 
                      success: function(data) {
                        it_works = data;
                      }
                    });
                   return it_works;
                }
            }
        }, 
        height: 450, 
        sortable: true, 
        pageable: {
            refresh: true 
        },
        columns: [  {
            field: "nombre_periodo",
            title: "Nombre del periodo",
            width: 400,
        },{
            field: "rango_inicial",
            title: "Rango inicial",
            width: 200,
        },{
            field: "rango_final",
            title: "Rango final",
            width: 200,
        },
        {command:[
            {
            name: "Editar",
                click: function (e) {
                    var tr = $(e.target).closest("tr"); // get the current table row (tr)
                    var data = this.dataItem(tr);
                    
                    $("#nombre_periodo").val(data.nombre_periodo); 
                    $("#rango_inicial").val(data.rango_inicial); 
                    $("#rango_final").val(data.rango_final);
                   
                    
                     $("#id_periodo").val(data.id_periodo);                             
                    
                    // Oculta mensajes de validaci√≥n de formulario
                   // validator.hideMessages();

                    // Copia un objeto sin referencias
                    objItem = JSON.parse(JSON.stringify(data));

                    // Abre ventana de formulario
                    var popup = $("#windowFormItem").data("kendoWindow");
                    //var ancho = (anchoVentana * 0.9);
                    popup.setOptions({
                        title: 'Editar inventario',
                        width: 600
                    });
                    $("#windowFormItem").data("kendoWindow").center().open();
                }
            },
            {
            	name: "Eliminar",
                title: "Eliminar",
                click: function (e){
                	e.preventDefault();
                    var tr = $(e.target).closest("tr"); 
                    var dat = this.dataItem(tr); 
                    var id = dat.id; 
                    if (confirm("¿Desea eliminar el archivo?")) {
	                    $.ajax({
	                    	url: "<?php echo admin_url('admin-ajax.php'); ?>",
								type: "POST",
								data: { action: 'delete_attachment', id_attachment: id },
								success: function (data){
                                   // $('.k-loading-mask').addClass("display-nones");
									$('#grid').data('kendoGrid').dataSource.read();
									$('#grid').data('kendoGrid').refresh();
										console.log(data);

                                    if(data == ""){
                                        alert("Archivo eliminado correctamente");
                                    }else{
                                        alert("No se eliminó el archivo");
                                    }   
								}
	                    });
                    }//if confirm   
	              }
                }],
    	}],
        dataBound: function (e) {
            $(".k-grid-Editar, .k-grid-Eliminar").css("margin-left", "30px");
        }
    });
});
</script>
<?php get_footer();?>