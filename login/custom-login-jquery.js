// A $( document ).ready() block.
$( document ).ready(function() {
    console.log( "ready!" );
    $('input[type=password]').removeClass("input").addClass('form-control col-md-12 col-lg-12');
    $('input[type=text]').removeClass("input").addClass('form-control');
    $( 'input[type=submit]' ).removeClass("button button-primary button-large").addClass( "button-submit" );
});
 