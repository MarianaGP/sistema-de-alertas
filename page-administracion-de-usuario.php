<?php get_header();

//Traigo los datos de usuario loggeado
$usser= wp_get_current_user();
$datos_usser = get_currentuserinfo();

//Extraigo datos del usuario
$id = $usser->id;
$pass = $usser ->user_pass_md5;

//Llamo a la funcion que trae datos de la tabla _wpusermeta la columna municipio y le paso el id del usuario 
$correo_ad = get_the_author_meta('correo_adicional', $user = $id ); 
$telefono_ad = get_the_author_meta('telefono_adicional', $user = $id ); 
$nombre_us = get_the_author_meta('first_name', $user = $id ); 

?> 

<div class= "container">
	<form class="marg-bottom-file" id="form-usuario">
		<div class ="adm-usuario">
			<fieldset>
				<legend>Administración de Usuario</legend>
					<form id = "admon-usuario">
						<div class = "container-usuario"> 
								<div class = "span-usuario">
									<input type="hidden" id="id" name="id" value="<?php echo $id; ?>" required>
									<label>Usuario: </label>
									<input type="text" id="usuario" name="usuario" value="<?php echo $nombre_us; ?>" required>
								</div>

								<div class = "span-usuario">
									<label>Contraseña: </label>
									<input type="password" id="contrasena" name="contrasena"  value="<?php echo $pass; ?>" class="span3" required>
								</div>

								<div class = "span-usuario">
									<label>Correo adicional: </label>
									<input type="text" id="correo" name="correo"  value="<?php echo $correo_ad; ?>" class="span3" required>
									<span id="correoOK"></span>
								</div>
								
								<div class = "span-usuario">
									<label>Teléfono: </label>
									<input type="text" id="telefono" name="telefono" value="<?php echo $telefono_ad; ?>" class="span3 solo-numero" maxlength="10" required>
								</div>

								<div class = "span12">
									<button type="submit" id="guardar" class="k-button-usuario" iconcls="k-icon k-i-search" >Guardar</button>
								</div>
								<div class="clearfix"></div>
								
						</div>
					</form>
				
			</fieldset>
		</div>
	</form>

</div>

<script type="text/javascript">
	//Para validar campo de teléfono solo sean números
	$('.solo-numero').keyup(function (){
    	this.value = (this.value + '').replace(/[^0-9]/g, '');
    }); 

	//Para validar correo
    document.getElementById('correo').addEventListener('input', function() {
	    campo = event.target;
	    valido = document.getElementById('correoOK');
	        
	    emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
	    //Se muestra un texto a modo de ejemplo, luego va a ser un icono
	    if (emailRegex.test(campo.value)) {
	      valido.innerText = "Correo válido";
	    } else {
	      valido.innerText = "Correo incorrecto";
	    }
	});

    $('#form-usuario').submit(function(event){
    	event.preventDefault();
    	var formData = new FormData($(this)[0]);
        formData.append('action', 'update_usuario');
            $.ajax({
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    console.log(data);
                    alert("Guardado correctamente");
                },
                error: function(jqXHR, textStatus, errorMessage) {
                   console.log(errorMessage); // Optional
                   alert("Error");
                }
            });

    }); 
</script>
<?php get_footer();?>