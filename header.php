<?php
global $wpdb;
$resultados= $wpdb->get_results( "SELECT nombre_municipio FROM municipio" );
?>
<!DOCTYPE html>
<html class="no-js">
<head>
	<meta charset="utf-8">
	<title>Sistema de Alertas | </title>
	<link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- General metadata -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow" />
    <meta name="author" content="#" />
    <meta http-equiv="content-type" content="text-html; charset=utf-8" />
    <link rel="icon" type="image/png" href="favicon.png" />
 
    <!-- Google -->
    <meta name="title" content="#" />
    <meta name="description" content="#" />
     <base href="http://demos.telerik.com/kendo-ui/grid/hierarchy">


    <!--Jquery-->
    <script src="<?php bloginfo('template_url'); ?>/js/jquery-3.1.0.min.js"></script>

    <!--complementos-->
    <script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script> 


    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/kendo.custom.css" /> 
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/kendo.common.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/kendo.rtl.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/kendo.mobile.all.min.css" />

    <script src="<?php bloginfo('template_url'); ?>/js/kendo.all.min.js"></script> 
    <script src="<?php bloginfo('template_url'); ?>/js/kendo.es-MX.js"></script> 




    <!--boostrapt-->
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
    <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <link href="<?php bloginfo('template_url'); ?>/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--picturefill-->
    <script src="<?php bloginfo('template_url'); ?>/js/picturefill.js"></script>

    <!--function js-->
    <script src="<?php bloginfo('template_url'); ?>/js/functions.js"></script>

    <!--progress -->
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.ajax-progress.js"></script>

    <!--function js-->
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.scrollto.js"></script>

    <!--style - principal-->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" />

    


</head>
 <script type="text/javascript">
        jQuery(document).ready(function(){
            $(document).on('touchstart click', '.button_link', function(event){
                event.preventDefault();
                $('#navmenu').stop().toggle('slow');
                if(!$(this).hasClass('close-icon')){
                    $(this).addClass('close-icon');
                }else{
                    $(this).removeClass('close-icon');
                }
            });

            $(document).on('touchstart click', '#navmenu .button_back', function(event){
                event.preventDefault();
                $('#navmenu').stop().toggle('slow');
            });         

            $(".nav-menu-xs a").click(function(evn){
            
                    evn.preventDefault();                    
                    $('html,body').scrollTo(this.hash, this.hash, { offset: { top:100 } }); 
                    $('#navmenu').stop().toggle('slow');
                    $('#responsive-menu-button').removeClass('close-icon'); 
               
                 
            });

            /*Funcion para cambiar header al realizar scroll*/ 
            window.onscroll = function() {myFunction()};

            function myFunction() {
                if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
                    document.getElementById("header-menu").className = "wrapper-header-scroll"; 
                } else {
                    document.getElementById("header-menu").className = "wrapper-header"; 
                }
            }
        });

         
    </script>
<body>   
    <div id="header-menu" class=" wrapper-header "> 
        <div class=" container padd-wrapper-nav"> 
            <div class="col-sm-12">
                <ul class="list-child" style="float: left; width: 100%;">
                    <li style="width: 80%;">
                        <a href="http://localhost/wordpress/" class="scroll font-neo" data-target="participantes" style="text-align: center;">Sistema para la Administración de Archivos de Municipios</a> 
                    </li> 
                    <li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23 dropdown" style="float: right;">
                        
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <img src="<?php bloginfo('template_url'); ?>/images/notificaciones.png" style= "width:30%">
                            <span class="badge">4</span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#"><i class="fa fa-fw fa-tag"></i><span class="badge">Mensaje</span> Conte </a></li>
                            <li><a href="#"><i class="fa fa-fw fa-thumbs-o-up"></i><span class="badge"> label text</span> Item </a></li>
                            <li><a href="#"><i class="fa fa-fw fa-thumbs-o-up"></i><span class="badge"> label text</span> Item </a></li>
                            <li><a href="#"><i class="fa fa-fw fa-thumbs-o-up"></i><span class="badge"> label text</span> Item </a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6"></div> 
         
            <a id="responsive-menu-button"  role="button" aria-label="Toggle Navigation" class="button_link lines-button x">
                <span class="lines"></span>
            </a>
            <nav id="navmenu" class="hidden-sm hidden-md hidden-lg">
                <div class="wrapper-nav-xs">
                    <ul>
                        <li>
                            <a href="">
                                Inicio
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Notificaciones
                            </a>
                        </li>
                        <li>
                            <a href="">Servicios</a>
                        </li>
                        <li>
                            <a href="">
                                Preguntas Frecuentes
                            </a>
                        </li>
                        <li>
                             <a href="">
                                 Contacto
                             </a>
                        </li>   
                    </ul>  
                    <div class="clearfix"></div> 
                </div>
            </nav> 
        </div> 
    </div> 
    <div class="container">
    
        <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs marg-logo">
            <img src="<?php bloginfo('template_url'); ?>/images/header/logo-veracruz.png">
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 hidden-xs">

           <div class="menu-menu1-container"><ul id="menu-menu1" class="nav navbar-nav nav-menu-header">
                <li id="menu-item-11" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-11">
                    <a href="http://localhost/wordpress/">Inicio</a>
                </li>
                <li id="menu-item-41" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-41"><a href="#">Servicios</a>
                <ul class="sub-menu">
                    <li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-17 current_page_item menu-item-60"><a href="http://localhost/wordpress/administracion-de-usuario/">Administracion de usuario</a></li>
                    <li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a href="http://localhost/wordpress/administracion-de-formatos/">Administracion de formatos</a></li>
                </ul>
                </li>
                <li id="menu-item-36" class="menu-item menu-item-type-post_type_archive menu-item-object-preguntas menu-item-36"><a href="http://localhost/wordpress/preguntas/">Preguntas Frecuentes</a></li>
                <li id="menu-item-12" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12"><a href="http://localhost/wordpress/contacto/">Contacto</a></li>
                </ul>
            </div>
        </div>

    </div>

    <div class="container sin-padding-xxs sin-padding-xs">
        <div class="border-header bg-header-single"></div>
        <div class="breadcrumbs">
            <ul id="breadcrumbs" class="breadcrumbs">
            </ul>   
         </div>
    </div>
