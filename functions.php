<?php

function upload_file(){
	
	
 	if ( ! function_exists( 'wp_handle_upload' ) ) 
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		
		$upload_overrides = array( 'test_form' => false, 'unique_filename_callback' => 'my_cust_filename' );
		
		add_filter( 'upload_dir', 'upload_dir_file' );
		$files = $_FILES['file_name'];
		foreach ($files['name'] as $key => $value) {
			if ($files['name'][$key]) {
				$uploadedfile = array(
					'name'     => $files['name'][$key],
					'type'     => $files['type'][$key],
					'tmp_name' => $files['tmp_name'][$key],
					'error'    => $files['error'][$key],
					'size'     => $files['size'][$key]
				);

				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				
				if ( $movefile && !isset( $movefile['error'] ) ) {
					if( empty( $ufiles ) ) $ufiles = array();
					$ufiles[] = $movefile;
				}
			}
		}
		remove_filter( 'upload_dir', 'upload_dir_file' ); 

		if ( $movefile ) {
				$attachment = array();
				for($i=0; $i<count($ufiles); $i++){
					$images[$i] = $ufiles[$i]['file'];
                }
                
                add_filter( 'upload_dir', 'upload_dir_file' );
                $path_upload = wp_upload_dir();
                $path_upload = $path_upload["path"];
                
                $wp_upload_dir = wp_upload_dir();

				foreach($images as $name) {
					$name_file = basename( $name );
					$name_file = pathinfo($name_file, PATHINFO_FILENAME);
				    $attachment = array(
				        'guid'=> $wp_upload_dir['url'] . '/' . basename( $name ), 
				        'post_mime_type' => 'image/png',
				        'post_title' =>  $name_file,
				        'post_content' => 'Archivo recibido',
				        'post_status' => 'inherit'
				         );

				    $image_id = wp_insert_attachment($attachment, $name);

				    require_once( ABSPATH . 'wp-admin/includes/image.php' );
				 
				    $attach_data = wp_generate_attachment_metadata( $image_id, $name );

				    wp_update_attachment_metadata( $image_id, $attach_data );

				}

                remove_filter( 'upload_dir', 'upload_dir_file' );
			} else {
				echo "No hubo datos adjuntos";
			}   
		die();
}
add_action('wp_ajax_upload_file', 'upload_file');
add_action('wp_ajax_nopriv_upload_file', 'upload_file');

function my_cust_filename($dir, $name, $ext){
    //Traigo los datos de usuario loggeado
	$cu= wp_get_current_user();
	//Extraigo el id del usuario
	$id = $cu->id;
	//Llamo a la funcion que trae datos de la tabla _wpusermeta la columna municipio y le paso el id del usuario 
	$trim = $_POST['periodo'];
	$municipio = get_the_author_meta('municipio', $id );
	$ext = ".xls";
	$data_array = $_POST;
	$year= date("Y");

	$name = 'VERACRUZ_' . $municipio . '_' . $year . '_' . $trim . $ext;
    return $name;
}

function upload_dir_file( $dirs ) {
	$cu= wp_get_current_user();
	$id = $cu->id;
	//Llamo a la funcion que trae datos de la tabla _wpusermeta la columna municipio y le paso el id del usuario 
	$municipio = get_the_author_meta('municipio', $user = $id );

	$dirs['subdir'] = '/upload_test/' .  $municipio;
	$dirs['path'] = $dirs['basedir'] . '/upload_test/' . $municipio;
	$dirs['url'] = $dirs['baseurl'] . '/upload_test/' . $municipio;

	return $dirs;
}


register_nav_menus(array(
	'primary'=>__('Menu Principal','sistema alertas'),
	)
);

/*Funcion para llamar al banner de anuncios*/
require_once (get_stylesheet_directory().'/theme-options/theme-options.php');

/*Funcion para llamar el formulario de contacto (Custom-Metas)*/
require_once (get_stylesheet_directory().'/dashboard-parts/custom-meta-postype.php');

/*Funcion para llamar el formulario de notificaciones (Custom-Metas)*/
require_once (get_stylesheet_directory().'/dashboard-parts/custom-meta-postype_noti.php');
 
 
/*Se agrega la función de Preguntas*/
add_action('init', 'init_custom_type');
function init_custom_type(){	
	$labels = array(
		'name' => 'Preguntas',
		'singular_name' => 'Preguntas',
		'add_new' => 'Agregar pregunta',
		'edit_item' => 'Editar pregunta',
		'new_item' => 'Nueva pregunta',
		'view_item' => 'Ver preguntas',
		'search_items' => 'Buscar pregunta',
		'not_found' =>  'No se encontraron preguntas',
		'not_found_in_trash' => 'No hay preguntas en la papelera', 
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
		//'register_meta_box_cb' => 'add_reporte_metaboxes',
		'has_archive' =>true
	); 
	register_post_type('preguntas',$args);
}

 
/*filter by users*/
add_action('pre_get_posts','users_own_attachments');
function users_own_attachments( $wp_query_obj ) {

    global $current_user, $pagenow;

    $is_attachment_request = ($wp_query_obj->get('post_type')=='attachment');

    if( !$is_attachment_request )
        return;

    if( !is_a( $current_user, 'WP_User') )
        return;

    if( !in_array( $pagenow, array( 'upload.php', 'admin-ajax.php' ) ) )
        return;

    if( !current_user_can('delete_pages') )
        $wp_query_obj->set('author', $current_user->ID );

    return;
}

function delete_attachment(){
	//$_POST['id'];
	$id = $_POST['id_attachment'];
	$delete = wp_delete_attachment($id, true);
	die();
}
add_action('wp_ajax_delete_attachment', 'delete_attachment');
add_action('wp_ajax_nopriv_delete_attachment', 'delete_attachment');

/***********************************/
/* Añade custom meta a perfil user */
/***********************************/

add_action( 'show_user_profile', 'custom_user_fields' );
add_action( 'edit_user_profile', 'custom_user_fields' );
add_action( "user_new_form", 'custom_user_fields' );


function custom_user_fields( $user ) { 
global $wpdb;
$resultados= $wpdb->get_results( "SELECT nombre_municipio FROM municipio" );
$resulareas= $wpdb->get_results( "SELECT nombre_area, id_area FROM area" );
?>


	<h3>Información Adicional</h3>

	<table class="form-table">
		<tr>
			<th><label for="municipio">Municipio: </label></th>

			<td>
				<select type="text" name="municipio" id="municipio" class="regular-text">  
				<option>
					-Seleccione municipio-
				</option>
				<?php 
				for($x=0; $x < count($resultados); $x++) {?>
					<option <?php echo ($resultados[$x]->nombre_municipio == get_the_author_meta( 'municipio', $user->ID )) ? "selected = 'selected'" : "";?> value ="<?php echo $resultados[$x]->nombre_municipio; ?>"> <?php echo $resultados[$x]->nombre_municipio; ?> </option>				
					<?php
				}
				?>
					
				<select/><br />
				<span class="description">Por favor coloque el municipio al que pertenece</span>
			</td>
		</tr>
		<tr>
			<th><label for="area">Área: </label></th>

			<td>
				<select type="text" name="area" id="area" class="regular-text">  
				<option>
					-Seleccione área-
				</option>
				<?php 
				for($x=0; $x < count($resulareas); $x++) {?>
					<option <?php echo ($resulareas[$x]->id_area == get_the_author_meta( 'area', $user->ID )) ? "selected = 'selected'" : "";?> value ="<?php echo $resulareas[$x]->id_area; ?>"> <?php echo $resulareas[$x]->nombre_area; ?> </option>				
					<?php
				}
				?>
					
				<select/><br />
				<span class="description">Por favor coloque el area al que pertenece</span>
			</td>
		</tr>
		<tr>
			<th><label for="correo_adicional">Correo Adicional: </label></th>

			<td>
				<input type="text" name="correo_adicional" id="correo_adicional" value="<?php echo esc_attr( get_the_author_meta( 'correo_adicional', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Agregue un email válido para alertas</span>
			</td>
		</tr>
		<tr>
			<th><label for="telefono_adicional">Teléfono: </label></th>

			<td>
				<input type="text" name="telefono_adicional" id="telefono_adicional" value="<?php echo esc_attr( get_the_author_meta( 'telefono_adicional', $user->ID ) ); ?>" class="regular-text" maxlength="10"/><br />
				<span class="description">Agregue un teléfono válido</span>
			</td>
		</tr>

	</table>
<?php }

add_action( 'personal_options_update', 'save_custom_user_fields' );
add_action( 'edit_user_profile_update', 'save_custom_user_fields' );
add_action( 'user_register', 'save_custom_user_fields');

function save_custom_user_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_usermeta( $user_id, 'area', $_POST['area'] );
	update_usermeta( $user_id, 'municipio', $_POST['municipio'] );
	update_usermeta( $user_id, 'correo_adicional', $_POST['correo_adicional'] );
	update_usermeta( $user_id, 'telefono_adicional', $_POST['telefono_adicional'] );
}

function update_usuario() {

	$id = $_POST['id'];
	$pass = $_POST['contrasena'];
	echo $pass;
	
	update_usermeta( $id, 'first_name', $_POST['usuario'] );
	wp_set_password( $pass, $id);
	update_usermeta( $id, 'correo_adicional', $_POST['correo'] );
	update_usermeta( $id, 'telefono_adicional', $_POST['telefono'] );
	die();
}

add_action('wp_ajax_update_usuario', 'update_usuario');
add_action('wp_ajax_nopriv_update_usuario', 'update_usuario');


/***********************************/
/*	Añade columna custom meta user */
/***********************************/

function add_user_columns($column) {
    $column['municipio'] = 'Municipio';
    return $column;
}
add_filter( 'manage_users_columns', 'add_user_columns' );

//add the data
function add_user_column_data( $val, $column_name, $user_id ) {
    $user = get_userdata($user_id);

    switch ($column_name) {
        case 'municipio' :
            return $user->municipio;
            break;
        default:
    }
    return;
}
add_filter( 'manage_users_custom_column', 'add_user_column_data', 10, 3 );

/***************************/
/****Customizar el login ***/
/***************************/

function my_custom_login() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-styles.css" />';
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/bootstrap.min.css" />';
	echo '<script src="' . get_bloginfo('stylesheet_directory') . '../js/jquery-3.1.0.min.js"></script>';
}
add_action('login_head', 'my_custom_login');

function my_custom_login_footer() { 
	echo '<script src="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-jquery.js"></script>';
}
add_action('login_footer', 'my_custom_login_footer');

  

/***************************/
/****Scripts view users  ***/
/***************************/
add_action( 'admin_enqueue_scripts', 'admin_theme_users' );
function admin_theme_users(){
	$selectOption = $_POST['municipio'];
	//echo "Hola".$selectOption;

	global $wpdb;
	global $pagenow;
	if($pagenow != "user-new.php"){
		return;
	}
	wp_register_script( 'script_user', get_template_directory_uri().'/js/functions.js' );
	wp_enqueue_script( 'script_user' );
}


?>