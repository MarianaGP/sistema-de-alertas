<?php get_header();
//Traigo los datos de usuario loggeado
$cu= wp_get_current_user();

//Extraigo el id del usuario
$id = $cu->id;
//
//Llamo a la funcion que trae datos de la tabla _wpusermeta la columna municipio y le paso el id del usuario 
$municipio = get_the_author_meta('municipio', $user = $id );
$id_area = get_the_author_meta('area', $user = $id );
//echo $municipio;

global $wpdb;
$resultados= $wpdb->get_results( "SELECT *FROM periodo where id_area = $id_area" );

?>

<div class="container"> 
	<form class="marg-bottom-file" id="form-file">
        <div id="ejemplo" >
            <div class = "span-usuario">
                <label for="periodo">Periodo: </label>
                <!--<select type="text" name="trimestre" id="trimestre"  class="regular-text">  
                    <option value = "">-Seleccione trimestre-</option>
                    <option value = "1TRIM"> 1er Trimestre</option>
                    <option value = "2TRIM"> 2do Trimestre</option>
                    <option value = "3TRIM"> 3er Trimestre</option>
                    <option value = "4TRIM"> 4to Trimestre</option>
                </select> -->
                <select type="text" name="periodo" id="periodo" class="regular-text">  
                <option>
                    -Seleccione periodo-
                </option>
                <?php 
                for($x=0; $x < count($resultados); $x++) {?>
                    <option <?php echo ($resultados[$x]->nombre_periodo == get_the_author_meta( 'periodo', $id )) ? "selected = 'selected'" : "";?> value ="<?php echo $resultados[$x]->value; ?>"> <?php echo $resultados[$x]->nombre_periodo; ?> </option>             
                    <?php
                }
                ?>
                    
                <select/>  
            </div>
            <div class = "span-usuario">
                <input name='file_name[]' id="files" type="file" aria-label="files" />
            </div>
            <input type="submit" value="Registrar Archivos " class="k-button-archivo" />     
        </div>
    </form>
	<div id="grid"></div>
</div>
<script type="text/x-kendo-template" id="template">
    <form name="formFac" id="formFac" action="" method="POST">
        <div class="toolbar" align="center"> 
            <a role = "button" class="k-button k-button-icontext k-grid-eliminar" href='#:url#'>
                Descargar  
            </a>  
        </div>
    </form>
</script>
<script type="text/javascript">  
	 
	$("#files").kendoUpload({
		async: {
		    saveUrl: "save",
            //autoUpload:false
		    //removeUrl: "http://my-app.localhost/remove"
		}, 
        upload: onUpload
	});

     // create ComboBox from select HTML element
    $("#trimestre").kendoDropDownList();

 

     function onUpload(e) {
        // An array with information about the uploaded files
        var files = e.files;
 
        // Checks the extension of each file and aborts the upload if it is not .jpg
        $.each(files, function () { 
            if (this.extension.toLowerCase() != ".xls") {
                if (this.extension.toLowerCase() != ".xlsx") {
                    alert("Solo archivos con extensión .xlsx y .xls pueden guardarse");
                    e.preventDefault();
                    return;
                }
            }
        });  
    }

    

    $('#form-file').submit(function(event){
        var trimestre= $('#trimestre').data("kendoDropDownList").value();

        if (trimestre=== '') {
            alert('Debes seleccionar un trimestre');
            return;
        }
        //Se recuperan datos del input file
        var upload = $("#files").data("kendoUpload"),
        files = upload.getFiles();

        if(files == ""){
            alert('Debes seleccionar un archivo');
             return;
        }

        event.preventDefault();

        var formData = new FormData($(this)[0]);
        formData.append('action', 'upload_file');
            $.ajax({
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    console.log(data);
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();
                    $(".k-upload-files.k-reset").find("li").remove();
                    $(".k-widget.k-upload").find("ul").remove();
                    $(".k-upload-files.k-reset").find("li").parent().remove();
                    if(data == ""){
                        alert("Archivo registrado correctamente");  
                    }                
                },
                error: function(jqXHR, textStatus, errorMessage) {
                   console.log(errorMessage); // Optional
                }
            });
        }); 

   

$(document).ready(function () {  

	$("#grid").kendoGrid({
        dataSource: {
            sync: function(){
                this.read();
            },
            transport: {
                read: {
                    url: "/wordpress/api-formatos/?acc=data",
                    type: "GET",
                    dataType: "json" 
                }
            },
            pageSize: 10,
            schema: {
                type: "json",
                data: "data",
                model: {
                    id: "id",
                    fields: {
                        nombre: {type: "string"}
                    }
                },
                total: function(e) {
                   // handle event
                   var it_works = 0;
                   $.ajax({
                      type: 'GET',
                      url: '/wordpress/api-formatos/?acc=total',
                      async: false, 
                      success: function(data) {
                        it_works = data;
                      }
                    });
                   return it_works;
                }
            }
        }, 
        height: 450, 
        sortable: true, 
        pageable: {
            refresh: true 
        },
        columns: [  {
            field: "nombre",
            title: "Nombre del archivo",
            width: 600,
        },{
            field: "fecha",
            title: "Fecha de Registro",
            width: 200,
        },{
            field: "url",
            title: " ",
            template: kendo.template($("#template").html())
        },
        {command:[{
            	name: "Eliminar",
                title: "Eliminar",
                click: function (e){
                	e.preventDefault();
                    var tr = $(e.target).closest("tr"); 
                    var dat = this.dataItem(tr); 
                    var id = dat.id; 
                    if (confirm("¿Desea eliminar el archivo?")) {
	                    $.ajax({
	                    	url: "<?php echo admin_url('admin-ajax.php'); ?>",
								type: "POST",
								data: { action: 'delete_attachment', id_attachment: id },
								success: function (data){
                                   // $('.k-loading-mask').addClass("display-nones");
									$('#grid').data('kendoGrid').dataSource.read();
									$('#grid').data('kendoGrid').refresh();
										console.log(data);

                                    if(data == ""){
                                        alert("Archivo eliminado correctamente");
                                    }else{
                                        alert("No se eliminó el archivo");
                                    }   
								}
	                    });
                    }//if confirm   
	              }
                }],
    	}],
        dataBound: function (e) {
            $(".k-grid-Editar, .k-grid-Eliminar").css("margin-left", "30px");
        }
    });
});
</script>
<?php get_footer();?>