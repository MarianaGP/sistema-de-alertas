<?php include('header.php'); 
//Traigo los datos de usuario loggeado
$cu= wp_get_current_user();

//Extraigo el id del usuario
$id = $cu->id;
//
//Llamo a la funcion que trae datos de la tabla _wpusermeta la columna municipio y le paso el id del usuario 
$municipio = get_the_author_meta('municipio', $user = $id );
$id_area = get_the_author_meta('area', $user = $id );
//echo $municipio;

global $wpdb;
$resultados= $wpdb->get_results( "SELECT *FROM periodo where id_area = $id_area" );

?>
<div class="container">
   <?php 
	   $args=array(
	   	'post_type'=>'post',
	   	'post_per_page' => 2
	   	);

	   $loop=new WP_Query($args);
	   if($loop->have_posts()){
	   	while ($loop-> have_posts()) {
	   		$loop->the_post();
	   		the_title();
	   		the_content();
	   		# code...
	   	}
	   }

	   
   ?>
   <?php
   	   $banners_sidebar = get_option("themeoption_banners_sidebar");
	   //print_r($banners_sidebar);
   ?>

   <?php
    $count_file = count($banners_sidebar['image_file']);
    for($i=0; $i < $count_file; $i++){
    ?>
    <div class="wrapper-notas-sidebar">
		   	<div id="page" class="sin-padding col-xs-12 col-sm-11 col-md-8 col-lg-8">
			   <div class="ultimas-noticias">
					<?php
						$args = array(
							'pagename' => 'mensaje-inicio'
						);
						$query = new WP_Query($args);
					    if($query->have_posts()){
					        while($query->have_posts()){
						        $query->the_post();   
						        the_content();
					        }
					    }
					?>
					<div id = "semfr" class ="semaforo">

							<div class ="container sin-padding">
								<div class ="col-md-2 pull left">
									<div class="form-group no-margen">
										<label class ="control-label">No entregado</label><br>
										<i class="fa fa-circle fa-3x c-red" aria-hidden="true"></i>
									</div>
								</div>
								<div class ="col-md-3">
									<div class="form-group no-margen">
										<label class ="control-label">Entregado después de fecha límite</label><br>
										<i class="fa fa-circle fa-3x c-yellow" aria-hidden="true"></i>
									</div>
								</div>
								<div class ="col-md-2">
									<div class="form-group no-margen">
										<label class ="control-label">Entregado</label><br>
										<i class="fa fa-circle fa-3x c-green" aria-hidden="true"></i>
									</div>
								</div>
							</div>

							<div class ="container sin-padding-index">
				            	<div class ="col-md-3 pull left">
				            		<div class="form-group no-margen">
					            		<label for="periodo">Año: </label>
						                <select type="text" name="anio" id="anio" class="regular-text">  
						                <option>
						                    -Seleccione año-
						                </option>
						                <option value = "2017"> 2017</option>
						                <option value = "2018"> 2018</option>
						                <select/>  
						            </div>
					            </div>
					            <div class ="col-md-3">
					            	<div class="form-group no-margen">
						                <label for="periodo">Periodo: </label>
						                <select type="text" name="periodo" id="periodo" class="regular-text">  
						                <option>
						                    -Seleccione periodo-
						                </option>
						                <?php 
						                for($x=0; $x < count($resultados); $x++) {?>
						                    <option <?php echo ($resultados[$x]->nombre_periodo == get_the_author_meta( 'periodo', $id )) ? "selected = 'selected'" : "";?> value ="<?php echo $resultados[$x]->value; ?>"> <?php echo $resultados[$x]->nombre_periodo; ?> </option>             
						                    <?php
						                }
						                ?>
						                    
						                <select/>  
						            </div>
					            </div>

				                
				            </div>

							<div id = "grid">

							</div>
					</div>

			   </div>
			</div>
	        <div class="banner-sidebar nota-item-g-right col-xs-12 col-sm-11 col-md-4 col-lg-4">
	        	<div class="item-banner-atencion">
	    			<a href="#">AVISOS</a>
	    			<div class="clearfix"></div>
		    	</div>
				<?php if(!empty($banners_sidebar['image_url'][$i])){ ?>
	            	<a href="<?php echo $banners_sidebar['image_url'][$i]; ?>" target="_blank">
	            		<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
	            	</a>
				<?php } else{ ?>
	            	<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
	            <?php } ?>
	        </div>
    </div>
    <?php } ?>
</div>
<script type="text/javascript">

var urlEnlace = "";
var urlAdmin = "";

	 $(document).ready(function () {
		 $("#grid").kendoGrid({
	        dataSource: {
	            type: "odata",
	            transport: {
	                read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Customers"
	            },
	            pageSize: 20
	        },
	        height: 550,
	        //groupable: true,
	        sortable: true,
	        pageable: {
	            refresh: true,
	            pageSizes: true,
	            buttonCount: 5
	        },
	        columns: [{
	            template: "<div class='customer-photo'" +
	                            "style='background-image: url(../content/web/Customers/#:data.CustomerID#.jpg);'></div>",
	            field: "ContactName",
	            title: "Estatus",
	            width: 80,
	        }, {
	            field: "ContactTitle",
	            title: "Formato",
	            width: 400,
	        }, {
	            field: "CompanyName",
	            title: "Fecha"
	        }]
	    });
	});
</script>
<?php include('footer.php'); ?>
