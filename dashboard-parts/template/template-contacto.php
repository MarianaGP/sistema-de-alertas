<?php
	$nombre = get_post_meta($post->ID, 'nombre', true);
	$cargo= get_post_meta($post->ID, 'cargo', true);
	$telefono= get_post_meta($post->ID, 'telefono', true);
    $email= get_post_meta($post->ID, 'email', true);
    $dependencia= get_post_meta($post->ID, 'dependencia', true);

?>
<div class="item-form">
	<div class="col-10">
        <div class="form-group">
            <label for="nombre" class="col-2 control-label">Nombre</label>
            <div class="col-7">
				<input type="text" name="nombre" value="<?php echo $nombre;?>">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <label for="cargo" class="col-2 control-label">Cargo</label>
            <div class="col-7">
            	<input type="text" name="cargo" value="<?php echo $cargo;?>">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <label for="telefono" class="col-2 control-label">Telefono</label>
            <div class="col-7">
                <input type="text" name="telefono" value="<?php echo $telefono;?>">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <label for="email" class="col-2 control-label">E-mail</label>
            <div class="col-7">
                <input type="text" name="email" value="<?php echo $email;?>">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <label for="dependencia" class="col-2 control-label">Dependencia</label>
            <div class="col-7">
                <input type="text" name="dependencia" value="<?php echo $dependencia;?>">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>