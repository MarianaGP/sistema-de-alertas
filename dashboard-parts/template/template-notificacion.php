 <?php
    $data_notificacion = get_post_meta($post->ID, 'data_notificacion', true);
    //echo "data: ".$data_notificacion;
?>
<div class="item-form">
	<div class="col-10">
        <div class="form-group">
            <div class="row">
                <div id="grid"></div>
            </div> 
        </div>
        <input type="hidden" name="data_notificacion" id="data_notificacion">
    </div>
    <div class="clearfix"></div>
</div>