// JavaScript Document
var Theme_init = {
	usuarios: jsonusuarios.usuarios,
	init : function(){
		Theme_init.kendoInit(); 
    },
	kendoInit: function(){
		 
 		var itemsSeleccionados = [];
 		var usuarios=JSON.parse(this.usuarios);
 
 		var dataSource = new kendo.data.DataSource({
            data: usuarios, 
            //group: [{field: "municipio"}],
            pageSize:7,
            schema : {
                type: "json",
                //data: "Item",
                model: {
                    fields: {
                    	id:"idusuario",
                        email: { field: "email", type: "string" },
                        usuario: { field: "usuario", type: "string" },
                        municipio: { field: "municipio", type: "string" },
                        status: { field: "status", type: "boolean" }
                    }  
                }
            }
        });

        var grid = jQuery("#grid").kendoGrid({
            dataSource: dataSource, 
            filterable:{
                        extra: false,
                        operators: {
                            string: {
                                startswith: "Empieza con",
                                eq: "Igual a",
                            }
                        }
                    },
            pageable: true, 
            sortable: {
                mode: "multiple",
                allowUnsort: true
            },
            columns: [
               // { field: "checkbox", title: " ", filterable: false, sortable: true, width: 50, template: "<input class='checkbox' type='checkbox' name='seleccionar' value='on' />", attributes: {style: "text-align: center" } },
                { selectable: true, width: "50px" },
                { field: "municipio", title: "Municipio", filterable: true, sortable: true,width: 150},
                { field: "email", title: "Correo", filterable: true, sortable: true },
            ]   
        });
            
        //bind click event to the checkbox
        grid.on("click", ".checkbox" , selectRow);  

        //Marca las filas que trae guardadas en el editar (Selecciona el checkbox)
        var view = dataSource.view(); 
        for(var j = 0; j < view.length; j++){ 
            if(view[j].status){ 
                //Se agregan al array de items seleccionados los preguardados (al momento de la edicion)
                itemsSeleccionados.push({
                    idusuario:view[j].id,
                    usuario:view[j].usuario,
                    correo: view[j].email,
                    municipio:view[j].municipio
                });
                jQuery("#grid").find("tr[data-uid='" + view[j].uid + "']").addClass("k-state-selected").find(".checkbox").prop("checked", true);
             }else{ 
                jQuery("#grid").find("tr[data-uid='" + view[j].uid + "']").removeClass("k-state-selected").find(".checkbox").prop("checked", false);
                itemsSeleccionados.splice(j, 1);

            } 
        }  
        
        //Función al hacer clic en un checkbox
        function selectRow() {
            var checked = this.checked,
                row = jQuery(this).closest("tr"),
                grid = jQuery("#grid").data("kendoGrid"),
                dataItem = grid.dataItem(row);

            var email=dataItem.email;
 
            if (checked) {
                //-select the row
                row.addClass("k-state-selected");
                //gurda cada fila que se selecciona en el array
                itemsSeleccionados.push({
                	idusuario:dataItem.id,
                    usuario:dataItem.usuario,
                    correo: dataItem.email,
                    municipio:dataItem.municipio
                }); 
            } else {
                
                //-remove selection
                row.removeClass("k-state-selected");

 				for(var i=0;i < itemsSeleccionados.length; i++) {
				  if(itemsSeleccionados[i].correo === email) {
                    //elimina del arreglo la fila des-seleccionada				     
                    itemsSeleccionados.splice(i, 1);
                      
				  }
				}
            }  
            jQuery('#data_notificacion').val(JSON.stringify(itemsSeleccionados));
         }

         
	} 
} 
jQuery( document ).on( 'ready', Theme_init.init );
