<?php 
/*************************
 *Load styles and scripts*
 *************************/
function add_admin_scripts_post( $hook ) {
    global $post;
    //$id = $post->ID;
	//$slug = basename( get_permalink( $id ) );
    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
		if ( 'page' === $post->post_type) {
			wp_enqueue_script('script_ui', get_stylesheet_directory_uri().'/dashboard-parts/js/jquery-ui.min.js' );
			
			/* wp_register_script( 'script_box', get_template_directory_uri().'/dashboard-parts/js/script-dashboard.js' );
			wp_enqueue_script( 'script_box' ); */

			/*$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
			wp_localize_script( 'script_box', 'theme_name', $translation_array );*/
            
            $slugPage = array( 'slugPage' => $slug );
			wp_localize_script( 'script_box', 'page_name', $slugPage );

            
		}

		if('notificaciones'===$post->post_type){
			wp_enqueue_script('script_ui', get_stylesheet_directory_uri().'/dashboard-parts/js/kendo.all.min.js' );
			wp_enqueue_script('script_ui', get_stylesheet_directory_uri().'/dashboard-parts/js/kendo.es-MX.js' );
			wp_register_script( 'script_box', get_template_directory_uri().'/dashboard-parts/js/script-dashboard.js' );
			wp_enqueue_script( 'script_box' );
			wp_enqueue_style( 'style_generic_kendo_common', get_stylesheet_directory_uri().'/dashboard-parts/css/kendo.common.min.css' );
			wp_enqueue_style( 'style_generic_kendo_custom', get_stylesheet_directory_uri().'/dashboard-parts/css/kendo.custom.css' );
			wp_enqueue_style( 'style_generic_kendo_rtl', get_stylesheet_directory_uri().'/dashboard-parts/css/kendo.rtl.min.css' );

			$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
			wp_localize_script( 'script_box', 'theme_name', $translation_array );

			$usuarios = get_users('orderby=nicename');
			
			$datos = array(); 

			$data_notificacion = get_post_meta($post->ID, 'data_notificacion', true);
			$data_notificacion = json_decode($data_notificacion);

			//Creacion del array de notificaciones
			$ArrayNotificaciones = array();
			foreach ($data_notificacion as $data) {
				$ArrayNotificaciones[]=$data->correo;
			}

			echo($ArrayNotificaciones); 

			for($i=0; $i < count($usuarios); $i++){
				//Declaracion de variable email para utilizarla en la comparacion del array
				$email=$usuarios[$i]->data->user_email;

				$data = array();
				$data['idusuario']=$usuarios[$i]->data->ID;
			    $data['usuario']=$usuarios[$i]->data->display_name;
			    $data['email']=$usuarios[$i]->data->user_email;
			    $data['municipio']= get_the_author_meta('municipio', $usuarios[$i]->data->ID );

			    //Se valida si el email que se esta recorriendo existe entro del Array de los correos que tienen notificaciones
				for($j=0; $j < count($data_notificacion); $j++){
			    	
				    if($data_notificacion[$j]->correo == $email){ 
				    	$data['status'] = true;  
				    break;
				    }else{
				    	$data['status'] = false;  
				    }
				}
 
				$datos[] = $data;

				//Asignacion de otro Array con los  mismos parametros pero con el correo adicional
				$correo_adicional=get_the_author_meta('correo_adicional', $usuarios[$i]->data->ID );
				//Se verifica si el usuario al que se esta recorriendo tiene correo adicional se agrega un nuevo registro
				if(isset($correo_adicional) && !empty($correo_adicional)){
				    $data = array();

				    $data['idusuario']=$usuarios[$i]->data->ID;
				    $data['usuario']=$usuarios[$i]->data->display_name;
				    $data['email']=$correo_adicional;
				    $data['municipio']=get_the_author_meta('municipio', $usuarios[$i]->data->ID );

				    for($k=0; $k < count($data_notificacion); $k++){
				    	
					    if($data_notificacion[$k]->correo == $correo_adicional){ 
					    	$data['status'] = true;  
					    	break;
					    }else{ 
					    	$data['status'] = false;  
					    }
					} 

				    $datos[] = $data;
				}
			} 

			$jsonusuarios= json_encode($datos); 
			//echo $jsonusuarios;
			$translation_array2 = array( 'usuarios' => $jsonusuarios ); 
			wp_localize_script( 'script_box', 'jsonusuarios', $translation_array2 );
 		}

		wp_enqueue_style('style_generic', get_stylesheet_directory_uri().'/dashboard-parts/css/style-dashboard.css' );
	}
}
add_action( 'admin_enqueue_scripts', 'add_admin_scripts_post', 10, 1 );


/*******************************
 * Add custom-meta - contacto *
 *******************************/
function add_custom_meta_contacto(){
	global $post;
	$id = $post->ID;
	$slug = basename( get_permalink( $id ) );
	if ( 'contacto' == $slug) {
		add_meta_box( 'meta_home_contacto', 'Contacto', 'meta_box_contacto', 'page', 'normal', 'high' );
		remove_post_type_support('page', 'editor');
	}
}
add_action( 'add_meta_boxes', 'add_custom_meta_contacto' );

function meta_box_contacto(){ 
	global $post;
	echo '<input type="hidden" name="meta_noncename" id="meta_noncename" value="'.wp_create_nonce(plugin_basename(__FILE__)).'" />';
	get_template_part( 'dashboard-parts/template/template', 'contacto' );
}


/*******************************
 * Add custom-meta - contactar *
 *******************************/


function meta_box_save_contactar( $post_id, $post ){
	if(!wp_verify_nonce($_POST['meta_noncename'], plugin_basename(__FILE__))){
			return $post->ID;
	}
	if(!current_user_can('edit_post', $post->ID))
		return $post->ID;
	$type=$_POST['post_type'];  
	$meta = array();
	switch($type){
		case 'page':
			$meta['nombre'] = $_POST['nombre'];
			$meta['cargo'] = $_POST['cargo'];
			$meta['telefono'] = $_POST['telefono'];
			$meta['email'] = $_POST['email'];
			$meta['dependencia'] = $_POST['dependencia'];

		break;
	}
	foreach($meta as $key => $value){
		if(is_array($meta) || is_object($meta)){
			if(get_post_meta($post->ID, $key, FALSE)){
				update_post_meta($post->ID, $key, $value);
			}else{ 
				add_post_meta($post->ID, $key, $value);
			}
			if(!$value) delete_post_meta($post->ID, $key);
		}
	}
} 
add_action('save_post', 'meta_box_save_contactar',1,2);

function removeEmptyElements(&$element)
{
    if (is_array($element)) {
        if ($key = key($element)) {
            $element[$key] = array_filter($element);
        }

        if (count($element) != count($element, COUNT_RECURSIVE)) {
            $element = array_filter(current($element), __FUNCTION__);
        }

        $element = array_filter($element);

        return $element;
    } else {
        return empty($element) ? false : $element;
    }
}
?>
