// JavaScript Document
var Custom_meta = {
	//main function
	init : function(){
		Custom_meta.addMoreField()
				   .addMoreFieldHome()
				   .exec_dragdrop()
				   .color_pickers();
	},
	addMedia: function(click_media){
		var button = jQuery("#"+click_media);
		var send_attachment = wp.media.editor.send.attachment;
		wp.media.editor.send.attachment = function(props, attachment){
			button.html("<img src='"+attachment.url+"' width='100%'>");
			jQuery("input[name*='"+click_media+"']").val(attachment.url);
			
			if(attachment.url != ''){
				jQuery( '.remove-p-'+click_media).css('display', 'block');
			}else{
				jQuery( '.remove-p-'+click_media).css('display', 'none');
			}
			
			wp.media.editor.send.attachment = send_attachment;
		}
		wp.media.editor.open(button);
		return false;
		return this;
	},
	removeMedia: function(click_media){
		var button = jQuery("#"+click_media);
		jQuery(document).on('click', '#remove-'+click_media , function(event) {
			event.preventDefault();
			jQuery( "input[name*='"+click_media+"']" ).val('');
			button.html("Asignar imagen destacada");
			jQuery( '.remove-p-'+click_media).css('display', 'none');
		});
		return this;
	},
	addMoreField: function(){
		var addmoreitem = jQuery(document);
		var deleteitem =  jQuery(document);
		var uploadButton =  jQuery(document);
		
		uploadButton.on('click','.btn-upload', function(){
			var button = jQuery(this);
			var index = jQuery(this).data('index');

			var send_attachment = wp.media.editor.send.attachment;
			wp.media.editor.send.attachment = function(props, attachment){
				jQuery( '#data_file_'+ index ).val(attachment.url);
				wp.media.editor.send.attachment = send_attachment;
			}
			wp.media.editor.open(button);
			return false;	
		});
		
		addmoreitem.on('click', '.btn-add-more', function(){
			var num_sliders = jQuery("#content-form .item-form-rich").length;
			jQuery("#add-more").remove();
			var form_slider =   "<div class='item-form-rich' style='height: 115px'>";
					form_slider +=	"<div class='col-9'>";
					form_slider +=		"<div class='form-group'>";
					form_slider +=			"<label for='content' class='col-2 control-label'>Imagen</label>";
					form_slider +=			"<div class='col-8'>";
					form_slider +=				"<div class='col-8 sin-padding'>";
					form_slider +=					"<input type='text' class='form-upload input-sm' id='data_file_"+num_sliders+"' name='data_file[]'>";
					form_slider +=				"</div>"
					form_slider +=				"<div class='col-2 sin-padding'>";
					form_slider +=					"<input type='button' class='button-primary form-btn-upload btn-upload btn-style' value='Subir' id='uploadimage"+num_sliders+"' data-index='"+num_sliders+"'/>";
					form_slider +=				"</div>";
					form_slider +=			"</div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='form-group'>";
					form_slider += 			"<label for='content' class='col-2 control-label'>Url</label>";
					form_slider +=			"<div class='col-8'>";
					form_slider +=				"<input type='text' name='item_url[]' id='item_url_"+num_sliders+"' class='regular-text'/>"; 
					form_slider +=			"</div>";
					form_slider +=			"<div class='clearfix'></div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='form-group'>"
					form_slider +=			"<label for='title' class='col-2 control-label'>Clases</label>";
					form_slider +=			"<div class='col-8'>";
					form_slider +=				"<select name='item_class[]' id='item_class_"+num_sliders+"'>"
					form_slider +=					"<option value='col-sm-4'>";
					form_slider +=						"Img Chica";
					form_slider +=					"</option>";
					form_slider +=					"<option value='col-sm-8'>";
					form_slider +=						"Img grande";
					form_slider +=					"</option>";
					form_slider +=				"</select>";
					form_slider +=			"</div>";
					form_slider +=			"<div class='clearfix'></div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='clearfix'></div>";
					form_slider +=	"</div>";
					form_slider +=	"<div class='col-1'>";
					form_slider +=		"<p>"+num_sliders+"</p>";
					form_slider +=	"</div>";
					form_slider +=	"<div class='clearfix'></div>";
				form_slider +=  "</div>";
				form_slider += "<input type='button' id='add-more' class='btn-add-more' value='Agregar más'>";
				jQuery("#content-form").append(form_slider);
		});
		deleteitem.on('click', '.delete-item' ,function() { 
			jQuery(this).parent().remove();
		});
		return this;
	},
	addMoreFieldHome: function(){
		var addmoreitem = jQuery(document);
		var deleteitem =  jQuery(document);
		var uploadButton =  jQuery(document);
		
		uploadButton.on('click','.btn-upload', function(){
			var button = jQuery(this);
			var index = jQuery(this).data('index');

			var send_attachment = wp.media.editor.send.attachment;
			wp.media.editor.send.attachment = function(props, attachment){
				jQuery( '#data_file_'+ index ).val(attachment.url);
				wp.media.editor.send.attachment = send_attachment;
			}
			wp.media.editor.open(button);
			return false;	
		});
		
		addmoreitem.on('click', '.btn-add-more-home', function(){
			var num_sliders = jQuery("#content-form .item-form-rich").length;
			jQuery("#add-more-home").remove();
			var form_slider =   "<div class='item-form-rich' style='height: 115px'>";
					form_slider +=	"<a class='delete-item'>";
					form_slider +=		"<img src='/tienda/wp-content/themes/richandsmoky-v1/backend/images/btn-close.png'>";
					form_slider +=  "</a>";
					form_slider +=	"<div class='col-9'>";
					form_slider +=		"<div class='form-group'>"
					form_slider +=			"<label for='title' class='col-2 control-label'>Tipo Contenido</label>";
					form_slider +=			"<div class='col-8'>";
					form_slider +=				"<select name='item_contenido[]' id='item_contenido_"+num_sliders+"'>"
					form_slider +=					"<option value='imagen'>";
					form_slider +=						"Imagen";
					form_slider +=					"</option>";
					form_slider +=					"<option value='instagram'>";
					form_slider +=						"Instagram";
					form_slider +=					"</option>";
					form_slider +=					"<option value='tweet'>";
					form_slider +=						"Tweet";
					form_slider +=					"</option>";
					form_slider +=				"</select>";
					form_slider +=			"</div>";
					form_slider +=			"<div class='clearfix'></div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='form-group'>";
					form_slider +=			"<label for='content' class='col-2 control-label'>Imagen</label>";
					form_slider +=			"<div class='col-8'>";
					form_slider +=				"<div class='col-8 sin-padding'>";
					form_slider +=					"<input type='text' class='form-upload input-sm' id='data_file_"+num_sliders+"' name='data_file[]'>";
					form_slider +=				"</div>"
					form_slider +=				"<div class='col-2 sin-padding'>";
					form_slider +=					"<input type='button' class='button-primary form-btn-upload btn-upload btn-style' value='Subir' id='uploadimage"+num_sliders+"' data-index='"+num_sliders+"'/>";
					form_slider +=				"</div>";
					form_slider +=			"</div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='form-group'>";
					form_slider += 			"<label for='content' class='col-2 control-label'>Url</label>";
					form_slider +=			"<div class='col-8'>";
					form_slider +=				"<input type='text' name='item_url[]' id='item_url_"+num_sliders+"' class='regular-text'/>"; 
					form_slider +=			"</div>";
					form_slider +=			"<div class='clearfix'></div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='form-group'>";
					form_slider +=			"<label for='content' class='col-2 control-label'>Color - Tweet</label>"
					form_slider +=			"<div class='col-8'>";
					form_slider +=				"<div class='input-group color_pick'>";
					form_slider +=					"<input type='text' name='color_pick[]' class='form-control' id='color_pick_"+num_sliders+"'/>";
					form_slider +=					"<span class='input-group-addon'><i></i></span>";
					form_slider +=				"</div>";
					form_slider +=			"</div>";
					form_slider +=			"<div class='clearfix'></div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='clearfix'></div>";
					form_slider +=	"</div>";
					form_slider +=	"<div class='clearfix'></div>";
				form_slider +=  "</div>";
				form_slider += "<input type='button' id='add-more-home' class='btn-add-more-home' value='Agregar más'>";
				jQuery("#content-form").append(form_slider);
				
				Custom_meta.color_pickers();
		});
		deleteitem.on('click', '.delete-item' ,function() { 
			jQuery(this).parent().remove();
		});
		return this;
	},
	exec_dragdrop: function(){
		//jQuery(window).on('load', function(){
			jQuery( "#content-form" ).sortable({
			  placeholder: "ui-state-highlight"
			});
			jQuery( "#content-form" ).disableSelection();
		//});
		return this;
	},
	color_pickers: function(){
		jQuery('.color_pick').colorpicker();
	}
} 
jQuery( document ).on( 'ready', Custom_meta.init );
