
<?php 
/**********Funciones para las Notificaciones***********/

/*****************************************************/


add_action('init', 'init_custom_type_noti');
function init_custom_type_noti(){	
	 
	$labels = array(
		'name' => 'Notificaciones',
		'singular_name' => 'Notificaciones',
		'add_new' => 'Agregar Notificación',
		'edit_item' => 'Editar Notificación',
		'new_item' => 'Nueva Notificación',
		'view_item' => 'Ver Notificación',
		'search_items' => 'Buscar Notificación',
		'not_found' =>  'No se encontraron notificaciones',
		'not_found_in_trash' => 'No hay notificaciones en la papelera', 
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
		//'register_meta_box_cb' => 'add_reporte_metaboxes',
		'has_archive' =>true
	); 
	register_post_type('notificaciones',$args);
	//add_meta_box( 'meta_home_notificacion', 'Notificacion', 'meta_box_notificacion', 'page', 'normal', 'high' );

	//require_once (get_stylesheet_directory().'/dashboard-parts/custom-meta-postype.php');
}


/*******************************
 * Add custom-meta - contacto *
 *******************************/
function add_custom_meta_notificaciones(){
	global $post;
	$id = $post->ID;
	//$slug = basename( get_permalink( $id ) );
	//if ( 'notificacion' == $slug) {
		add_meta_box( 'meta_home_notificacion', 'Notificacion', 'meta_box_notificacion', 'notificaciones', 'normal', 'high' );
		//remove_post_type_support('page', 'editor');
	//}
}
add_action( 'add_meta_boxes', 'add_custom_meta_notificaciones' );

function meta_box_notificacion(){ 
	global $post;
	echo '<input type="hidden" name="meta_noncename" id="meta_noncename" value="'.wp_create_nonce(plugin_basename(__FILE__)).'" />';
	get_template_part( 'dashboard-parts/template/template', 'notificacion' );
}


/*******************************
 * Add custom-meta - contactar *
 *******************************/


function meta_box_save_notificaciones( $post_id, $post ){
	if(!wp_verify_nonce($_POST['meta_noncename'], plugin_basename(__FILE__))){
			return $post->ID;
	}
	if(!current_user_can('edit_post', $post->ID))
		return $post->ID;
	$type=$_POST['post_type'];  
	$meta = array();
	switch($type){
		case 'notificaciones':
			$meta['data_notificacion'] = $_POST['data_notificacion'];
		break;
	}
	foreach($meta as $key => $value){
		if(get_post_meta($post->ID, $key, FALSE)){
			update_post_meta($post->ID, $key, $value);
		}else{ 
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key);
	}
} 
add_action('save_post', 'meta_box_save_notificaciones',1,2);

 ?>