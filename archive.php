<?php get_header(); ?>
	
	<div class="clearfix"></div>
	<div class="container">

   <?php
   	   $banners_sidebar = get_option("themeoption_banners_sidebar");
	   //print_r($banners_sidebar);
   ?>

   <?php
    $count_file = count($banners_sidebar['image_file']);
    for($i=0; $i < $count_file; $i++){
    ?>
    <div class="wrapper-notas-sidebar">
		   	<div id="page" class="single-container col-md-8 col-lg-8">
			   <div class="preguntas-frecuentes">
			   	<h3>Preguntas Frecuentes</h3>
			   		<table style="border: 0px;padding: 5px;width: 100%">
			   			<tbody>
			   				<tr>
							<?php
							    if(have_posts()){
							        while(have_posts()){
							        the_post();   
							        the_title();
							        the_content();
							        }
							    }

							?>
							</tr>
						</tbody>
					</table>
			   </div>
			</div>
	        <div class="banner-sidebar nota-item-g-right col-xs-12 col-sm-11 col-md-4 col-lg-4">
	        	<div class="item-banner-atencion">
	    			<a href="#">AVISOS</a>
	    			<div class="clearfix"></div>
		    	</div>
				<?php if(!empty($banners_sidebar['image_url'][$i])){ ?>
	            	<a href="<?php echo $banners_sidebar['image_url'][$i]; ?>" target="_blank">
	            		<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
	            	</a>
				<?php } else{ ?>
	            	<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
	            <?php } ?>
	        </div>
    </div>
    <?php } ?>

	</div>
<?php get_footer(); ?>
