<?php include('header.php'); ?>
<div class="container">
   <?php 
	   $args=array(
	   	'post_type'=>'post',
	   	'post_per_page' => 2
	   	);

	   $loop=new WP_Query($args);
	   if($loop->have_posts()){
	   	while ($loop-> have_posts()) {
	   		$loop->the_post();
	   		the_title();
	   		the_content();
	   		# code...
	   	}
	   }

	   
   ?>
   <?php
   	   $banners_sidebar = get_option("themeoption_banners_sidebar");
	   //print_r($banners_sidebar);
   ?>

   <?php
    $count_file = count($banners_sidebar['image_file']);
    for($i=0; $i < $count_file; $i++){
    ?>
    <div>
	   	<div class="sin-padding col-xs-12 col-sm-11 col-md-8 col-lg-8">
	   		<div class="col-lg-5"></div>
		    <div class="tit-contacto col-lg-4">
				<?php
				    if(have_posts()){
				        while(have_posts()){
				        	$nombre = get_post_meta($post->ID, 'nombre', true);
							$cargo= get_post_meta($post->ID, 'cargo', true);
							$telefono= get_post_meta($post->ID, 'telefono', true);
						    $email= get_post_meta($post->ID, 'email', true);
						    $dependencia= get_post_meta($post->ID, 'dependencia', true);
					        the_post();   
					        the_title();
   				 ?>
	        </div>
	        <div class="col-lg-3"></div>
	        <div class="col-lg-12 pad-contacto"> 
	        	<div class="col-lg-3">
				</div>

				<div class="info-contacto col-lg-8"><i class="fa fa-user-circle-o fa-4x" aria-hidden="true"></i>
					<ul class="inline-block">
					 	<li class="contacto li-block">Nombre: <?php echo $nombre; ?></li>
					    <li class="contacto li-block">Cargo: <?php echo $cargo; ?></li>
					</ul>
				</div>

				<div class="col-lg-1">
				</div>

				<div class="clearfix"></div>

				<div class="col-lg-3">
				</div>

				<div class="info-contacto col-lg-8"><i class="fa fa-phone fa-4x" aria-hidden="true"></i>
					<ul class="inline-block">
					 	<li class="contacto li-block">Telefono: <?php echo $telefono; ?></li>
					</ul>
				</div>
				<div class="col-lg-1">
				</div>

				<div class="clearfix"></div>

				<div class="col-lg-3">
				</div>

				<div class="info-contacto col-lg-8"><i class="fa fa-envelope-o fa-4x" aria-hidden="true"></i>
					<ul class="inline-block">
					 	<li class="contacto li-block">E-mail: <?php echo $email; ?></li> 	
					</ul>
				</div>

				<div class="col-lg-1">
				</div>

				<div class="clearfix"></div>

				<div class="col-lg-3">
				</div>

				<div class="info-contacto col-lg-8"><i class="fa fa-building-o fa-4x" aria-hidden="true"></i>
					<ul class="inline-block">
					 	<li class="contacto li-block">Dependencia: <?php echo $dependencia; ?></li> 	
					</ul>
				</div>

				<div class="col-lg-1">
				</div>
				<?php   
				      }
				    } 
				?> 
		   </div>
		</div>
        <div class="nota-item-g-right col-xs-12 col-sm-11 col-md-4 col-lg-4">
        	<div class="item-banner-atencion">
    			<a href="#">AVISOS</a>
    			<div class="clearfix"></div>
	    	</div>
			<?php if(!empty($banners_sidebar['image_url'][$i])){ ?>
            	<a href="<?php echo $banners_sidebar['image_url'][$i]; ?>" target="_blank">
            		<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
            	</a>
			<?php } else{ ?>
            	<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
            <?php } ?>
        </div>
    </div>
    <?php } ?>
</div>
<?php include('footer.php'); ?>

 