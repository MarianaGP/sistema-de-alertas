<footer>

<script type="text/javascript">
    $(document).ready(function(){
      $('#footer-content .menu').each(function(index, element) {
        $(this).children('li').find('.current-menu-item').parent('.sub-menu').siblings('a').parent().addClass('hola');
      });
        });
  </script>
  <div id="footer-content" class="hidden-less-600 bg-gray-2d2d2d">
    <div class="container-slider content-logo-menu">
      <div class="content-menu">
          <div class="content-footer-menu col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
            <div class="menu-main-menu-container">
              <ul id="menu-main-menu-1" class="menu hidden-xs">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-236228"><a href="http://localhost/wordpress/">Inicio</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-301239"><a href="#">Servicios</a>
                  <ul class="sub-menu">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-169873"><a href="http://localhost/wordpress/administracion-de-usuario/">Administracion de usuario</a></li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-27506"><a href="http://localhost/wordpress/administracion-de-formatos/">Administracion de formatos</a></li>
                  </ul>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-236227"><a href="http://localhost/wordpress/preguntas/">Preguntas Frecuentes</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-169875"><a href="http://localhost/wordpress/contacto/">Contacto</a></li>
              </ul>
            </div>                    
          </div>
        </div>
        <div class="clearfix"></div>  
      </div>
    </div><!-- #footer-content -->
    <div class="clearfix"></div>
      <div class="containet-logos-footer bg-gray-2d2d2d hidden-lg"></div>
    <div class="title-page-footer">
      <p>VERACRUZ<span class="gob-mx">.GOB.MX</span></p>
    </div>
    <div class="container-slider hidden-less-600">
      <div class="content-copyright">
        <p>Secretaría de Finanzas y Planeacion. 
        Av. Xalapa No. 301 Col. Unidad del Bosque C.P. 91010 Xalapa, Ver. Tel. (228) 842-1400. </p>
      </div>
    </div>
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '#code', 'auto');
  ga('send', 'pageview');

</script>
</body>

</html>
