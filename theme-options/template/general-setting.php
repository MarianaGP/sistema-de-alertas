<?php 
	$general_settings = get_option("themeoption_general_setting");
?>
<div class="title-section">
	<h3>General settings</h3>    
</div>
<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="logo-principal" class="control-label">
            	Logo principal
        </label>
        <span class="description-title">
            Logo principal de veracruz, o Secretaría
            (Ancho maximo 255px).
        </span>
    </div>
    <div class="col-lg-9">
    	<div class="widget-media">
			<?php if(isset($general_settings['logo-principal']) && !empty($general_settings['logo-principal'])){ ?>
                <a title="Agregar logo" id="logo-principal" onclick="Theme_init.addMedia('logo-principal')">
                    <img src='<?php echo $general_settings['logo-principal']; ?>' width='120px'>
                </a>
            <?php }else{ ?>
                <a title="Asignar logo" id="logo-principal" onclick="Theme_init.addMedia('logo-principal')">
                	<img src='<?php bloginfo('template_url'); ?>/theme-options/images/img-not-found.png' width='120px'>
                	<p>
                    	Asignar logo
                    </p>
                </a>
                
            <?php } ?>
            <p class="remove-p-logo-principal" <?php echo (isset($general_settings['logo-principal']) && !empty($general_settings['logo-principal'])) ? " " : "style=display:none;"; ?>>
                <a href="#" id="remove-logo-principal" onclick="Theme_init.removeMedia('logo-principal')">Quitar logo</a>
            </p>
            <input type="hidden" name="logo-principal" value="<?php echo $general_settings['logo-principal']; ?>">
    	</div>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="text-header-title" class="control-label">Texto para header</label>
        <span class="description-title">
            Descripción de el header (SEO).
        </span>
    </div>
    <div class="col-lg-9">
        <input type="text" class="form-control" id="text-header-title" name="text-header-title" value="<?php echo esc_attr($general_settings["text-header-title"]); ?>">
    </div>
</div>

<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="footter-description" class="control-label">Descripción del Footer</label>
        <span class="description-title">
            Escribe una descripción para el footer (copyright, o descripción).
        </span>
    </div>
    <div class="col-lg-9">
        <textarea class="form-control" id="footter-description" name="footter-description" rows="3"><?php echo $general_settings["footter-description"]; ?></textarea>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="code-google-analytics" class="control-label">ID Google Analytics</label>
        <span class="description-title">
            Copy paste your ID Account
            ejemplo: "UA-XXXXXXXX-1" 
        </span>
    </div>
    <div class="col-lg-9">
        <input class="form-control" id="code-google-analytics" name="code-google-analytics" value="<?php echo $general_settings["code-google-analytics"]; ?>">
    </div>
</div>

<!------------------------>

<div class="title-section">
    <h3>Redes sociales</h3>    
</div>

<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="facebook-url" class="control-label">Facebook Url</label>
        <span class="description-title">
            Escribe la url de tu cuenta de
            facebook 
        </span>
    </div>
    <div class="col-lg-9">
        <input class="form-control" id="facebook-url" name="facebook-url" value="<?php echo $general_settings["facebook-url"]; ?>">
    </div>
</div>
<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="twitter-url" class="control-label">Twitter Url</label>
        <span class="description-title">
            Escribe la url de tu cuenta de
            Twitter 
        </span>
    </div>
    <div class="col-lg-9">
        <input class="form-control" id="twitter-url" name="twitter-url" value="<?php echo $general_settings["twitter-url"]; ?>">
    </div>
</div>
<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="youtube-url" class="control-label">Youtube Url</label>
        <span class="description-title">
            Escribe la url de tu cuenta de
            Youtube
        </span>
    </div>
    <div class="col-lg-9">
        <input class="form-control" id="youtube-url" name="youtube-url" value="<?php echo $general_settings["youtube-url"]; ?>">
    </div>
</div>
<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="googleplus-url" class="control-label">Google Plus Url</label>
        <span class="description-title">
            Escribe la url de tu cuenta de
            Google Plus
        </span>
    </div>
    <div class="col-lg-9">
        <input class="form-control" id="googleplus-url" name="googleplus-url" value="<?php echo $general_settings["googleplus-url"]; ?>">
    </div>
</div>
<!------------------------>

<div class="title-section">
    <h3>Correo Electrónico</h3>    
</div>
<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="e-mail" class="control-label">Correo Electrónico</label>
        <span class="description-title">
            Escribe el e-mail en donde llegarán
            los mensajes de contactar y asegurate 
            de que sea una cuenta valida.
        </span>
    </div>
    <div class="col-lg-9">
        <input class="form-control" id="e-mail" name="e-mail" value="<?php echo $general_settings["e-mail"]; ?>">
    </div>
</div>