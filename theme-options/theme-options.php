<?php
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Load up the menu page
 */
function theme_options_add_page() {
	$menu = add_menu_page( __( 'Opciones de tema', 'sampletheme' ), __( 'Opciones generales', 'sampletheme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
	add_action( 'admin_print_styles-' . $menu, 'admin_theme_style' );
	add_action( 'admin_print_scripts-' . $menu, 'admin_theme_script' );
}
function admin_theme_style(){
	wp_enqueue_style( 'style_theme_option', get_template_directory_uri().'/theme-options/style.css');
}
function admin_theme_script(){
	wp_enqueue_script( 'script_theme_boostrap', get_template_directory_uri().'/js/bootstrap.min.js');
	wp_enqueue_media();
	wp_enqueue_script( 'editor' );
	
	wp_register_script( 'script_theme_functions', get_template_directory_uri().'/theme-options/js/functions.js' );
	wp_enqueue_script( 'script_theme_functions' );
	
	$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
	//after wp_enqueue_script
	wp_localize_script( 'script_theme_functions', 'theme_name', $translation_array );
}

function theme_options_do_page() {
	?>
	<div class="wrap">
		<div class="col-lg-12">
			<?php echo "<h2 class='title-theme-option'>" . wp_get_theme() . __( ' (Opciones del tema)', 'sampletheme' ) . "</h2>"; ?>
        </div>
		<form class="theme_options" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="theme-options-panel">
			<?php get_template_part( 'theme-options/content', 'template' ); ?> 
        </form>
	</div>
	<?php
}

function themeoption_save(){
	$banners_sidebar = array(
		"image_file" => array_filter($_POST["image_file"]), 
		"image_url"  => array_filter($_POST["image_url"])
	);
	
	echo json_encode($_POST);
	update_option("themeoption_banners_sidebar", array_merge($banners_sidebar));
	exit();
}
add_action('wp_ajax_themeoption_save', 'themeoption_save');

add_action('wp_ajax_themeoption_sites_save', 'themeoption_sites_save');

function theme_after_setup_theme(){
	if(!get_option($themename . "_installed")){		
		
		$general_settings = array(
			"logo-url-main" => $_POST["logo-url-main"],
		);
		$slider_option = array(
			"slider1" => "Inspiration",
		);
		
		add_option($themename . "_general_setting", $general_settings);
		add_option($themename . "_installed", 1);
	}
}
add_action("after_setup_theme", "theme_after_setup_theme");

function theme_switch_theme($theme_template){
	global $themename;
	delete_option($themename . "_installed");
}
add_action("switch_theme", "theme_switch_theme");