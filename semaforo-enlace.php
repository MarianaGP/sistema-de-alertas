<?php include('header.php'); ?>
<div class="container">
   <?php 
	   $args=array(
	   	'post_type'=>'post',
	   	'post_per_page' => 2
	   	);

	   $loop=new WP_Query($args);
	   if($loop->have_posts()){
	   	while ($loop-> have_posts()) {
	   		$loop->the_post();
	   		the_title();
	   		the_content();
	   		# code...
	   	}
	   }

	   
   ?>
   <?php
   	   $banners_sidebar = get_option("themeoption_banners_sidebar");
	   //print_r($banners_sidebar);
   ?>

   <?php
    $count_file = count($banners_sidebar['image_file']);
    for($i=0; $i < $count_file; $i++){
    ?>
    <div class="wrapper-notas-sidebar">
		   	<div id="page" class="sin-padding col-xs-12 col-sm-11 col-md-8 col-lg-8">
			   <div class="ultimas-noticias">
					<?php
						$args = array(
							'pagename' => 'mensaje-inicio'
						);
						$query = new WP_Query($args);
					    if($query->have_posts()){
					        while($query->have_posts()){
						        $query->the_post();   
						        the_content();
					        }
					    }
					?>
			   </div>
			</div>
	        <div class="banner-sidebar nota-item-g-right col-xs-12 col-sm-11 col-md-4 col-lg-4">
	        	<div class="item-banner-atencion">
	    			<a href="#">AVISOS</a>
	    			<div class="clearfix"></div>
		    	</div>
				<?php if(!empty($banners_sidebar['image_url'][$i])){ ?>
	            	<a href="<?php echo $banners_sidebar['image_url'][$i]; ?>" target="_blank">
	            		<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
	            	</a>
				<?php } else{ ?>
	            	<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
	            <?php } ?>
	        </div>
    </div>
    <?php } ?>
</div>
<?php include('footer.php'); ?>
